ifdef DOTENV
	DOTENV_TARGET=dotenv
else
	DOTENV_TARGET=.env
endif

##################
# PUBLIC TARGETS #
##################
test: $(DOTENV_TARGET)
	docker-compose run cfn-python-lint cfn-lint -t cloudformation.yml

deploy: $(DOTENV_TARGET)
	docker-compose run --rm stacker build stacker.yml --tail --recreate-failed

diff: $(DOTENV_TARGET)
	docker-compose run --rm stacker diff stacker.yml

###########
# ENVFILE #
###########
# Create .env based on .env.template if .env does not exist
.env:
	@echo "Create .env with .env.template"
	cp .env.template .env

# Create/Overwrite .env with $(DOTENV)
dotenv:
	@echo "Overwrite .env with $(DOTENV)"
	cp $(DOTENV) .env

$(DOTENV):
	$(info overwriting .env file with $(DOTENV))
	cp $(DOTENV) .env
.PHONY: $(DOTENV)
